use strict;
use warnings;

use Test::More;
use Test::Command::Simple;

my $CMD = 'eye';

# network access may be absent or unreliable
plan skip_all => 'network test skipped when EXTENDED_TESTING is unset'
	unless ($ENV{EXTENDED_TESTING});

run_ok $CMD, qw(--n3 https://eyereasoner.github.io/eye/reasoning/socrates/socrates.n3 --query https://eyereasoner.github.io/eye/reasoning/socrates/socrates-query.n3);
like stdout, qr/r:because/, 'help, stderr';
like stderr, qr/starting .*\nGET .*\nnetworking .*\nreasoning/s, 'help, stderr';

done_testing;
