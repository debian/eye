use strict;
use warnings;

use Test::More;
use Test::Command::Simple;

my $CMD = 'eye';

run_ok $CMD;
cmp_ok stdout, 'eq', '', 'bare command, stdout';
like stderr, qr/Usage: $CMD/, 'bare command, stderr';

run_ok $CMD, '--help';
cmp_ok stdout, 'eq', '', 'help, stdout';
like stderr, qr/Usage: $CMD/, 'help, stderr';

run_ok $CMD, qw(--n3 reasoning/socrates/socrates.n3 --query reasoning/socrates/socrates-query.n3);
like stdout, qr/r:because/, 'help, stderr';
like stderr, qr/starting .*\nGET .*\nnetworking .*\nreasoning/s, 'help, stderr';

done_testing;
